# README #

Resolve errors from error_table.bin

* Requires os0:/kd/error_table.bin in the working directory

```
Arguments: <mode> <input>
        Modes:
        -d Decode hex to shortcode
        -b Bruteforce hex from shortcode
No arguments = interactive
```

Download: https://bitbucket.org/SilicaAndPina/errorresolver/downloads/ErrorResolver.exe